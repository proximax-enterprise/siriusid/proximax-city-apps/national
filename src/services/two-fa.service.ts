import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface TwoFA {
  id?: string;
  publicKey:string;
  name:string;
  account:string;
  secret:string;
  active:boolean;
}

@Injectable({
  providedIn: 'root'
})
export class TwoFaService {

  private twoFACollection: AngularFirestoreCollection<TwoFA>;
 
  private twoFA: Observable<TwoFA[]>;
 
  constructor(db: AngularFirestore) {
    this.twoFACollection = db.collection<TwoFA>('2fa');
 
    this.twoFA = this.twoFACollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc['id'];
          return { id, ...data };
        });
      })
    );
  }
 
  get2FAs() {
    return this.twoFA;
  }
 
  get2FA(id) {
    return this.twoFACollection.doc<TwoFA>(id).valueChanges();
  }
 
  update2FA(twoFA: TwoFA, id: string) {
    return this.twoFACollection.doc(id).update(twoFA);
  }
 
  add2FA(twoFA: TwoFA) {
    return this.twoFACollection.add(twoFA);
  }
 
  remove2FA(id) {
    return this.twoFACollection.doc(id).delete();
  }
}
