import { Injectable } from '@angular/core';
import { Address, NetworkType } from 'tsjs-xpx-chain-sdk';

@Injectable({
  providedIn: 'root'
})
export class ChangeDataService {

  pubickeySiriusid =''; 
  addressSiriusid = '';
  name = ''; // Name of user when him sign up
  
  /**
   * For TEST_NET
   */
  publicKeydApp = '4017DDCEED15C2A8F0032251017ED045322C418707003A63540A6F9942720A80'; 
  privateKeydApp = 'C8CE2F2E7E2AFA0BDC2BBECC4AA10A41508327BAE0A584985EA6EB4F5818BAC9'
  addressdApp: Address;
  apiNode = "";
  ws = 'wss://' + this.apiNode; //websocket
  //ws = "wss://bctestnet3.xpxsirius.io";
  
  /**
   * For local testing
   */
  // ws = 'ws://192.168.1.23:3000'; //local
  // publicKeydApp = '0750E2716F2CBADD19F9F5314944ABFDF005A5D3D6DAAB4D08E97F188AAA9300'; // privateKey: 4EBACFE2B723FDA8B4DFC7E7B8AA26AF9C8638DCC353463AFF89F03230AFD38E
  updateWebsocket(){
    this.ws = 'wss://' + this.apiNode;
  }
  constructor() { 
    this.addressdApp = Address.createFromPublicKey(this.publicKeydApp, NetworkType.TEST_NET);
    console.log("address is:",this.addressdApp);
  }
}
