import { Component, OnInit } from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
import {ClientInfoService} from '../../services/client-info.service';
import { ChangeDataService } from 'src/services/change-data.service';
import { Router } from '@angular/router';
import { Mosaic, MosaicId,Account,NetworkType } from 'tsjs-xpx-chain-sdk';
import {
  BlockchainNetworkConnection,
  ConnectionConfig,
  Protocol,
  IpfsConnection,
  Uploader,
  UploadParameter,
  BlockchainNetworkType,
  Uint8ArrayParameterData,
  StreamHelper,
  DownloadParameter,
  Downloader

} from "tsjs-chain-xipfs-sdk";
import { StorageBucketService } from '../services/storage-bucket/storage-bucket.service';
// import exportFromJSON from 'export-from-json';

var fs = require('fs');

@Component({
  selector: 'app-show-result',
  templateUrl: './show-result.page.html',
  styleUrls: ['./show-result.page.scss'],
})
export class ShowResultPage implements OnInit {

  fname;
  lname;
  birth;
  address;
  phoneNumber;
  email;
  motherName;
  fatherName;
  connect = false;
  subscribe;
  photoHash;
  photonew;
  
  

  constructor(
    private router: Router,
    public clientInfo: ClientInfoService,
    private changeDataService: ChangeDataService,
    private domSanitizer:DomSanitizer,
    private storageBucketService: StorageBucketService
  ) {
    let content = this.clientInfo.clientInfo[0].content;
    this.fname = content[0][1];
    this.lname = content[1][1];
    this.birth = content[2][1];
    this.address = content[3][1];
    this.phoneNumber = content[4][1];
    this.email = content[5][1];
    this.motherName = content[6][1];
    this.fatherName = content[7][1];
    // this.photoHash = content[8][1];
   }

  ngOnInit() {
  }

  ionViewWillEnter(){
    // this.continue();
    // this.getImage();
  }

  // dowload image

  async continue(){

    const sender = Account.createFromPrivateKey(this.changeDataService.privateKeydApp,NetworkType.TEST_NET);
    const BlockchainInfo = {
      apiHost: 'api-1.testnet2.xpxsirius.io',
      apiPort: 443,
      apiProtocol: 'https'
    };
    const IpfsInfo = {
      host: 'ipfs1-dev.xpxsirius.io',
      options: { protocol: 'https' },
      port: 5443
    };
    const connectionConfig = ConnectionConfig.createWithLocalIpfsConnection(
      new BlockchainNetworkConnection(
        BlockchainNetworkType.TEST_NET,
        BlockchainInfo.apiHost,
        BlockchainInfo.apiPort,
        Protocol.HTTPS
      ),
      new IpfsConnection(IpfsInfo.host, IpfsInfo.port,IpfsInfo.options)
    );
    console.log("connectionConfig",connectionConfig);
    const transactionHash = this.photoHash;
    const downloader = new Downloader(connectionConfig);
    const param = DownloadParameter.create(transactionHash).build();

    const result1 = await downloader.download(param);
    const result2 = await result1.data.getContentAsBuffer();
    const result3 = await document.createElement("img");
    result3.src = 'data:image/jpeg;base64,' + result2.toString("base64");
    this.photonew = result3.src;

  };

  async getImage() {
    let siriusIdPubKey = this.changeDataService.pubickeySiriusid;
    let imageUrl = await this.storageBucketService.getImageUrl(`${siriusIdPubKey}-img`);
    console.log(imageUrl);
    this.photonew = imageUrl;
  }


  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.router.navigate(['/check-result']);
  }

  applyForm(){
    this.router.navigate(['/application']);
  }

  exportCredential(){
    console.log('co vao day')
    const data = {
      'First Name': this.fname,
      'Last Name': this.lname,
      'Date of Birth': this.birth,
      'Address': this.address,
      'Phone Number': this.phoneNumber,
      'Email': this.email,
      'Mother\'s Name': this.motherName,
      'Father\'s Name': this.fatherName,
      // 'Photo': this.photonew,
    }
    const fileName = 'credential'
    const exportType = 'json'
 
    // exportFromJSON({ data, fileName, exportType })
  }
}
