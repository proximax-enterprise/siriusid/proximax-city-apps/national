import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { ChangeDataService } from 'src/services/change-data.service';
import { Listener, TransferTransaction, Deadline, EncryptedMessage, PublicAccount, NetworkType, Address, TransactionHttp, Account, UInt64 } from 'tsjs-xpx-chain-sdk';
import { LoginRequestMessage, VerifytoLogin, CredentialRequestMessage, Credentials, CredentialConfirmMessage, VerifytoConfirmCredential, ApiNode } from 'siriusid-sdk';
import { Router } from '@angular/router';
import { ClientInfoService } from 'src/services/client-info.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from "@ionic-native/file-transfer/ngx";
import { File } from "@ionic-native/file/ngx";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { Platform } from "@ionic/angular";
import { StoredInfoService } from '../services/stored-info.service';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Mosaic, MosaicId } from 'tsjs-xpx-chain-sdk';
import {
  PrivacyType,
  BlockchainNetworkConnection,
  TransactionClient,
  ConnectionConfig,
  Protocol,
  BlockchainTransactionService,
  IpfsClient,
  IpfsConnection,
  Uploader,
  UploadParameter,
  BlockchainNetworkType,
  CreateProximaxDataService,
  Uint8ArrayParameterData,
  ProximaxDataModel
} from "tsjs-chain-xipfs-sdk";
import { Todo, TodoService } from 'src/services/todo.service';
import { TwoFaService } from 'src/services/two-fa.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { TwoFactorAuth } from 'siriusid-sdk';
import { StorageBucketService } from '../services/storage-bucket/storage-bucket.service';

@Component({
  selector: 'app-application',
  templateUrl: './application.page.html',
  styleUrls: ['./application.page.scss'],
})
export class ApplicationPage implements OnInit {

  @ViewChild(IonContent, { static: false }) ionContent: IonContent;

  currentNode = "api-1.testnet2.xpxsirius.io";
  subscribe;
  listener: Listener;
  connect = false;
  qrImg;
  showImg = false;
  qrCredential;
  qrCredentialTest; //for testing

  loading = false;
  linkLogin;
  linkCredential;

  fname;
  lname;
  birth;
  address;
  phoneNumber;
  email;
  motherName;
  fatherName;
  photo: string = null;
  // photonew;
  photoHash;
  fileUrl: any;//imageURI
  fileName: any;//imageFileName
  fileUploadOption;
  aggree = false;
  warning = false;
  warning2 = false;
  warning3;
  step1 = true;
  step2 = false;
  step3 = false;
  step4 = false;
  step5 = false;
  step6 = false;
  step7 = false;
  step1_1 = false;
  approved = false;
  todos: Todo[] = [];
  todoId = null;
  todo: Todo;
  twoFAs = [];
  active2FA;
  token;
  secret;
  btnCon = false;
  constructor(
    private camera: Camera,
    private transfer: FileTransfer,
    private file: File,
    private changeDataService: ChangeDataService,
    private router: Router,
    private clientInfo: ClientInfoService,
    public storedInfo: StoredInfoService,
    private filePath: FilePath,
    private platform: Platform,
    private webView: WebView,
    private todoService: TodoService,
    private twoFAService: TwoFaService,
    private storageBucketService: StorageBucketService
  ) {
    //this.confirmCredential();
    console.log('api node: ' + this.currentNode);
  }

  ngOnInit() {
    this.todoService.getTodos().subscribe(res => {
      console.log('received todos', res);
      this.todos = res;
    })

    this.twoFAService.get2FAs().subscribe(res => {
      console.log('received twoFAs', res);
      this.twoFAs = res;
    })
  }

  ionViewWillEnter() {
    ApiNode.apiNode = "https://" + this.currentNode;
    ApiNode.networkType = NetworkType.TEST_NET;
    this.changeDataService.apiNode = this.currentNode;
    this.changeDataService.updateWebsocket();
    this.listener = new Listener(this.changeDataService.ws, WebSocket);
    this.loginRequestAndVerify();
  }


  async loginRequestAndVerify() {
    this.connect = false;
    const loginRequestMessage = LoginRequestMessage.create(this.changeDataService.publicKeydApp, []);
    const sessionToken = loginRequestMessage.getSessionToken();
    this.qrImg = await loginRequestMessage.generateQR();
    this.linkLogin = await loginRequestMessage.universalLink();

    this.connection(sessionToken);
    let interval = setInterval(async () => {
      const listenerAsAny = this.listener as any;
      console.log(listenerAsAny.webSocket.readyState);
      if (listenerAsAny.webSocket.readyState !== 1 && listenerAsAny.webSocket.readyState !== 0 && !this.connect) { // 0 connecting, 1 connected, 3 disconnect
        this.warning2 = true;
        this.listener = new Listener(this.changeDataService.ws, WebSocket);
        console.log('ws is:', this.changeDataService.ws);
        this.connection(sessionToken);
        // this.showImg = false;
      }
      else if (listenerAsAny.webSocket.readyState == 1 && !this.connect) {
        this.showImg = true;

      }
      else if (this.connect) {
        // this.showImg = false;
        clearInterval(interval);
      }
    }, 1000);
  }

  // after scanning, pressed confirm, and 
  async connection(sessionToken) {
    this.listener.open().then(() => {
      this.warning2 = false;
      this.subscribe = this.listener.confirmed(this.changeDataService.addressdApp).subscribe(transaction => {
        VerifytoLogin.verify(transaction, sessionToken, [], this.changeDataService.privateKeydApp).then(
          async verify => {
            if (verify) {
              this.connect = true;
              console.log("Transaction matched");
              this.changeDataService.addressSiriusid = transaction.signer.address.plain();
              console.log(transaction);
              this.changeDataService.pubickeySiriusid = transaction.signer.publicKey;
              this.clientInfo.clientInfo = VerifytoLogin.credentials;

              if (this.check2FACreated() && this.active2FA) {
                console.log("kich hoat 2fa")
                this.step1 = false;
                this.step1_1 = true;
              }
              else {
                console.log("koooooooooo kich hoat 2fa")
                this.gotoNext();
              }


            }
            else console.log("Transaction not match");
          }
        )

      });
    })

  }

  async gotoNext() {
    if (await this.checkCredentialCreated()) {
      console.log("o the ah");
      if (this.approved) {
        this.step1 = false;
        this.step1_1 = false;
        this.step5 = true;
      }
      else {
        this.step1 = false;
        this.step1_1 = false;
        this.step6 = true;
      }
    }
    else {
      console.log("uhm the day");
      this.step1 = false;
      this.step1_1 = false;
      this.step2 = true;
    }
    this.subscribe.unsubscribe();
  }

  async checkCredentialCreated() {
    console.log("dm vao day nhe");
    console.log(this.todos);
    for (let i = 0; i < this.todos.length; i++) {
      if (this.todos[i].address == this.changeDataService.addressSiriusid && this.todos[i].dAppId == 'proximaxId') {
        this.approved = this.todos[i].approved;
        let credential = JSON.parse(this.todos[i].userData) as Credentials;
        let msg = CredentialRequestMessage.create(credential);
        console.log("check credential created");
        console.log(msg);
        this.qrCredential = await msg.generateQR();
        this.linkCredential = await msg.universalLink();

        return true;
      }
    }
    return false;
  }

  refresh() {
    this.subscribe.unsubscribe();
    this.loginRequestAndVerify();
  }
  async continue() {
    if (this.fname && this.lname && this.birth && this.address && this.phoneNumber && this.email
      && this.motherName && this.fatherName && this.aggree /*&& this.photonew*/ && !this.btnCon) {
      this.btnCon = true;
      this.scrollContent();
      this.loading = true;

      const transactionHttp = new TransactionHttp("https://api-1.testnet2.xpxsirius.io");

      const sender = Account.createFromPrivateKey(this.changeDataService.privateKeydApp, NetworkType.TEST_NET);

      const generationHash = "56D112C98F7A7E34D1AEDC4BD01BC06CA2276DD546A93E36690B785E82439CA9";

      // const BlockchainInfo = {
      //   apiHost: 'api-1.testnet2.xpxsirius.io',
      //   apiPort: 443,
      //   apiProtocol: 'https'
      // }
      // const IpfsInfo = {
      //   host: 'ipfs1-dev.xpxsirius.io',
      //   options: { protocol: 'https' },
      //   port: 5443
      // };
      // const connectionConfig = ConnectionConfig.createWithLocalIpfsConnection(
      //   new BlockchainNetworkConnection(
      //     BlockchainNetworkType.TEST_NET,
      //     BlockchainInfo.apiHost,
      //     BlockchainInfo.apiPort,
      //     Protocol.HTTPS
      //   ),
      //   new IpfsConnection(IpfsInfo.host, IpfsInfo.port,IpfsInfo.options)
      // );
      // console.log("connectionConfig",connectionConfig);
      // const upLoader= new Uploader(connectionConfig);


      // UPload file
      // console.log(this.photonew);
      // const image = this.convertDataURIToBinary(this.photonew); 
      // console.log(image);  // Uint8Array(5423)
      // const metaParams = Uint8ArrayParameterData.create(image);
      // const uploadParamsBuilder = UploadParameter.createForUint8ArrayUpload(metaParams, sender.privateKey).withTransactionMosaics([]).build();
      // console.log("metaParams", metaParams.contentType);
      // console.log("metaParams", metaParams);  //Uint8ArrayParameterData
      // console.log(uploadParamsBuilder);
      // console.log('uploader', upLoader);
      // const result1 = await upLoader.upload(uploadParamsBuilder);

      // this.photoHash = result1.transactionHash;
      // console.log("image", image);
      // console.log("Hash of photo:",this.photoHash);


      // // Upload to S3 bucket
      // let fileKey = this.changeDataService.pubickeySiriusid+"-img"
      // let fileExtension = ".jpeg";
      // await this.storageBucketService.postFile(this.photonew, fileKey, fileExtension);
      // console.log(fileKey);

      let content = new Map<string, string>([
        ['First Name', this.fname],
        ['Last Name', this.lname],
        ['Date of Birth', this.birth],
        ['Address', this.address],
        ['Phone Number', this.phoneNumber],
        ['Email', this.email],
        ["Mother's Name", this.motherName],
        ["Father's Name", this.fatherName],
        // ['Photo', fileKey]
      ])

      const credential = Credentials.create(
        'proximaxId',
        'ProximaXCity ID',
        'ProximaXCity ID Department',
        '/assets/icon-credentials-employment-pass.svg',
        [],
        content,
        "",
        Credentials.authCreate(content, this.changeDataService.privateKeydApp)
      );

      this.todo = {
        dAppId: 'proximaxId',
        createdAt: new Date().getTime(),
        userData: JSON.stringify(credential),
        address: this.changeDataService.addressSiriusid,
        publicKey: this.changeDataService.pubickeySiriusid,
        approved: true
      }
      this.saveTodo(this.todo).then((res) => {
        console.log('Save National ID success in database!')
      }).catch((err) => {
        console.log('Save fail!', err)
      })

      this.loading = false;
      this.warning = false;
      this.step2 = false;
      this.step4 = true;
    }
    else {
      this.btnCon = false;
      this.warning = true;
    }
  }

  async saveTodo(todo) {
    if (this.todoId) {
      this.todoService.updateTodo(todo, this.todoId).then(() => {
      });
    } else {
      this.todoService.addTodo(todo).then(() => {
      });
    }
  }

  finish() {
    this.step3 = false;
    this.step4 = true;
  }

  finish2() {
    this.step5 = false;
    this.step7 = true;
  }

  convertDataURIToBinary(dataURI: string) {
    const BASE64_MARKER = ';base64,';
    const base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
    const base64 = dataURI.substring(base64Index);
    const raw = window.atob(base64);
    const rawLength = raw.length;
    const array = new Uint8Array(new ArrayBuffer(rawLength));

    for (let i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }

  getImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
    this.camera.getPicture(options).then((imageData) => {
      if (this.platform.is("android")) {
        this.filePath.resolveNativePath(imageData).then(filePath => {
          let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
          let currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        });
      }
      else if (this.platform.is('ios')) {
        var currentName = imageData.substr(imageData.lastIndexOf('/') + 1);
        var correctPath = imageData.substr(0, imageData.lastIndexOf('/') + 1);
        this.copyFileToLocalDirOnIos(correctPath, currentName, this.createFileName());
      }

      else {
        console.log("Get picture of platform neither android nor ios")
        this.filePath.resolveNativePath(imageData).then(filePath => {
          let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
          let currentName = imageData.substring(imageData.lastIndexOf('/') + 1, imageData.lastIndexOf('?'));
          this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        });
      }
    }, (err) => {
      console.log(err);
    });
  }

  createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }


  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(
      success => {
        this.photo = this.storedInfo.avatar;
        this.storedInfo.setAvatar(this.webView.convertFileSrc(this.file.dataDirectory + newFileName));
      }, error => {
        console.log("Error while storing file.");

      }
    );
  }

  copyFileToLocalDirOnIos(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.photo = this.storedInfo.avatar;
      const win: any = window;
      const avatar = win.Ionic.WebView.convertFileSrc(this.file.dataDirectory + newFileName);
      this.storedInfo.setAvatar(avatar);
    }, error => {
      console.log('Error while storing file.');
    });
  }

  previewFile($event) {
    const preview = <HTMLInputElement>document.getElementById('upload');//querySelector('img');
    const file = $event.target.files[0];
    const reader = new FileReader();
    reader.onload = (e) => {
      // convert image file to base64 string
      preview.src = reader.result as string;
      // this.photonew = reader.result;
    };

    if (file) {
      reader.readAsDataURL(file);
    }
  }
  checkResult(param: string) {
    if (param == "hard") {
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light") {
      this.clientInfo.lightVerification = true;
    }
    this.connect = true;
    this.subscribe.unsubscribe();
    this.step1 = true;
    this.step2 = false;
    this.step3 = false;
    this.step4 = false;
    this.fname = '';
    this.lname = '';
    this.birth = '';
    this.address = '';
    this.phoneNumber = '';
    this.email = '';
    this.motherName = '';
    this.fatherName = '';
    this.router.navigate(['/check-result']);
  }

  scrollContent() {
    this.ionContent.scrollToTop(300);
  }

  deeplink(param: string) {
    if (param == "login") {
      window.location = this.linkLogin
    }
    else {
      window.location = this.linkCredential
    }
  }

  twoFA() {
    this.router.navigate(['/two-fa']);
  }

  check2FACreated() {
    console.log("dm vao day nhe");
    console.log(this.twoFAs);
    for (let i = 0; i < this.twoFAs.length; i++) {
      if (this.twoFAs[i].publicKey == this.changeDataService.pubickeySiriusid) {
        this.active2FA = this.twoFAs[i].active;
        this.secret = this.twoFAs[i].secret.secret;
        return true;
      }
    }
    return false;
  }

  handle2FA() {
    let obj = TwoFactorAuth.verifyToken(this.secret, this.token);
    if (obj == null) {
      this.warning3 = "Wrong 2FA Code!"
    }
    else if (obj.delta == -1) {
      this.warning3 = "Old 2FA Code!"
    }
    else if (obj.delta == 0) {
      console.log('EVERYTHING\'s fine');
      this.warning3 = null;
      this.gotoNext();
    }
  }
}
