import { TestBed } from '@angular/core/testing';

import { StoredInfoService } from './stored-info.service';

describe('StoredInfoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StoredInfoService = TestBed.get(StoredInfoService);
    expect(service).toBeTruthy();
  });
});
