import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StoredInfoService {

  avatar:string;

  constructor(  
    private storage:Storage,
    private sanitize: DomSanitizer,
    private plaform:Platform) {
      this.storage.get("avatar").then((val)=>{
        if(val){
          this.avatar = val;}
        else this.avatar = "assets/user.png"
      })
     }

     setAvatar(avatar:string){
      this.avatar=avatar;
    }
    
    getAvatar(){
      if(this.plaform.is("android")){
        return this.avatar;
      } else if(this.plaform.is('ios')){
        return this.sanitize.bypassSecurityTrustResourceUrl(this.avatar);
      } else 
      {return this.avatar;
      console.log("Platform is not android or ios.")}
  
    }
}
