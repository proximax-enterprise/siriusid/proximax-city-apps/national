import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { PhotoLibrary } from '@ionic-native/photo-library/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {CacheModule } from 'ionic-cache';
import { FileTransfer,FileTransferObject,FileUploadOptions} from "@ionic-native/file-transfer/ngx";
import { File } from "@ionic-native/file/ngx";
import { IonicStorageModule } from '@ionic/storage';
import { Camera} from "@ionic-native/camera/ngx";
import { StoredInfoService } from './services/stored-info.service';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    BrowserModule, 
    IonicModule.forRoot(), 
    CacheModule.forRoot(), 
    AppRoutingModule,
    IonicStorageModule.forRoot()],
  
  providers: [
    {provide:LocationStrategy,
    useClass:HashLocationStrategy},
    Camera,
    FileTransfer,
    FileTransferObject,
    AngularFirestoreModule,
    // FileUploadOptions,
    File,
    FilePath,
    WebView,
    StoredInfoService,
    PhotoLibrary,
    StatusBar,
    SplashScreen,
    // { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
