import { Component, OnInit } from '@angular/core';
import { TwoFaService } from 'src/services/two-fa.service';
import {TwoFAMessage, TwoFactorAuth} from 'siriusid-sdk';
import { ChangeDataService } from 'src/services/change-data.service';
import { ThrowStmt } from '@angular/compiler';
import { ClientInfoService } from 'src/services/client-info.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-two-fa',
  templateUrl: './two-fa.page.html',
  styleUrls: ['./two-fa.page.scss'],
})
export class TwoFaPage implements OnInit {

  step1 = true;
  step2 = false;
  name;
  account;
  twoFA;
  qrImg;
  active;
  id;
  warning;
  init = true;
  showQRCode = false;
  secret;
  constructor(
    private twoFAService: TwoFaService,
    private changeDataService: ChangeDataService,
    public clientInfo: ClientInfoService,
    private router:Router
  ) { 
    console.log("vao day r");
  }

  ngOnInit() {
    this.twoFAService.get2FAs().subscribe(res=>{
      this.twoFA = res;

      if(this.init && this.check2FACreated()){
        this.step1 = false;
        this.step2 = true;
        this.init = false;
      }
      else if (this.init && !this.check2FACreated()){
        this.step1 = true;
        this.step2 = false;
        this.init = false;
      }
    })
  }

  async active2FA(){
    if (this.name && this.account){
      console.log("createaa nhe");
      let mess = TwoFAMessage.create(this.name,this.account);
      console.log(TwoFactorAuth.secret);
      this.active = true;
      let data = {
        publicKey:this.changeDataService.pubickeySiriusid,
        name:this.name,
        account:this.account,
        secret:TwoFactorAuth.secret,
        active:this.active
      }
  
      console.log(data);
  
      this.save2FA(data).then((res)=>{
        console.log('Save National ID success in database!')
      }).catch((err)=>{
        console.log('Save fail!',err)
      })
      this.showQRCode = true;
      this.qrImg = await mess.generateQR();
  
      
      this.step1 = false;
      this.step2 = true;
    }
    else this.warning = true;
    
  }

  enable(bool){
    console.log("turn on / off");
    this.active = bool;
    console.log(this.active);
    let data = {
      publicKey:this.changeDataService.pubickeySiriusid,
      name:this.name,
      account:this.account,
      secret:this.secret,
      active:this.active
    }
    console.log(data);
    console.log(this.id);
    this.save2FA(data).then((res)=>{
      console.log('Save National ID success in database!')
    }).catch((err)=>{
      console.log('Save fail!',err)
    })
  }

  async save2FA(data) { 
    if (this.step2) {
      console.log("update lllll");
      this.twoFAService.update2FA(data, this.id).then(() => {
      });
    } else {
      console.log("create newwww lllll");
      this.twoFAService.add2FA(data).then(() => {
      });
    }
  }

  check2FACreated(){
    console.log("dm vao day nhe");
    console.log(this.twoFA);
    for (let i=0;i<this.twoFA.length;i++){
      if (this.twoFA[i].publicKey == this.changeDataService.pubickeySiriusid){
        this.active = this.twoFA[i].active;
        this.id = this.twoFA[i].id;
        this.name = this.twoFA[i].name;
        this.account = this.twoFA[i].account;
        this.secret = this.twoFA[i].secret;
        console.log("check credential created");
        console.log(this.twoFA[i]);
        return true;
      }
    }
    return false;
  }

  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.router.navigate(['/check-result']);
  }
}
