import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoFaPage } from './two-fa.page';

describe('TwoFaPage', () => {
  let component: TwoFaPage;
  let fixture: ComponentFixture<TwoFaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoFaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoFaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
