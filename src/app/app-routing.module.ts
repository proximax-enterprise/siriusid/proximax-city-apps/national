import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { ApplicationPage } from './application/application.page';
import { CheckResultPage } from './check-result/check-result.page';
import { ShowResultPage } from './show-result/show-result.page';

const routes: Routes = [
  { path: '', redirectTo: 'application', pathMatch: 'full' },
  // { path: 'application', loadChildren: () => import('./application/application.module').then( m => m.ApplicationPageModule)},
  {  path: 'application', loadChildren: './application/application.module#ApplicationPageModule'},
  { path: 'check-result', loadChildren: './check-result/check-result.module#CheckResultPageModule' },
  { path: 'show-result', loadChildren: './show-result/show-result.module#ShowResultPageModule' },
  { path: 'two-fa', loadChildren: './two-fa/two-fa.module#TwoFaPageModule' },
  // {  path: 'application', component: ApplicationPage},
  // { path: 'check-result', component: CheckResultPage},
  // { path: 'show-result', component: ShowResultPage},
  { path: '**', redirectTo: 'application', pathMatch: 'full' },
  
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
