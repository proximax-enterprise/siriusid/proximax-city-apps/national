// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

// export const environment = {
//   production: false,
//   firebase:{
//     apiKey: "AIzaSyBqMBd85rMh3l0WozVfFR4-ST7nCHQKKSU",
//     authDomain: "loginfirebaseadminpage.firebaseapp.com",
//     databaseURL: "https://loginfirebaseadminpage.firebaseio.com",
//     projectId: "loginfirebaseadminpage",
//     storageBucket: "loginfirebaseadminpage.appspot.com",
//     messagingSenderId: "923549361938",
//     appId: "1:923549361938:web:f9a19532644108b2cc75cb",
//     measurementId: "G-4E0HJYXRV3"
//   }
// };

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyAC6zPZ-UKH1KTeb0ZI73vIikhfhoBZcWw",
    authDomain: "proximax-city-id.firebaseapp.com",
    projectId: "proximax-city-id",
    storageBucket: "proximax-city-id.appspot.com",
    messagingSenderId: "986442270185",
    appId: "1:986442270185:web:92454a0242a1d576d67778"
  },
  S3Credentials: {
    username: "",
    password: ""
  }
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
